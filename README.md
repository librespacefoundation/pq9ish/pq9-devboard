# PQ9 Testing Board

![PQ9 Testing Board image](pq9-devboard.png "PQ9 Testing Board")

This is a simple testing board with breakouts for 2 PQ9 Standard boards

## Dependencies

This design uses the Libre Space Foundation KiCAD Library [lsf-kicad-lib](https://gitlab.com/librespacefoundation/lsf-kicad-lib#usage). Follow [this guide](https://gitlab.com/librespacefoundation/lsf-kicad-lib#usage) for local usage.

## Instructions
Use jumpers to configure the connectivity on the board. Be careful on P5 connector that can be used for common routing of voltages.


## License
&copy; 2017 Libre Space Foundation & commiters

Licensed under the [CERN OHLv1.2](LICENSE).
